* [ ] Find Speaker
* [ ] Find Venue
* [ ] Organise Catering
* [ ] Create speaker Slides in [Google Drive](https://drive.google.com/drive/folders/1VxV7GUTlahlxiPJ_iI0g-Cz7CWiHnMNq)
* [ ] Create Header Images with a logo and the Speaker's Foto for Meetup and Instagram (in google slides)

### Metup.com

* [ ] Create Event on [meetup.com](https://www.meetup.com/Kotlin-Vienna/schedule/)
  * [ ] Add topic description
  * [ ] Add correct direction
  * [ ] Add Speaker Bio
  * [ ] Upload event specific banner image
* [ ] Announce the Meetup on meetup.com (between 6 and 2 weeks before the event)

### Social

* [ ] Upload Instagram post for the between 12 and 2 weeks before the event
  * [ ] Add topic description
* [ ] Schedule a twitter post 7 days and 1 day before the event with [tweetdeck](https://tweetdeck.twitter.com/)
  * [ ] Add the actual topic to the post
  * [ ] Add Instagram banner to the scheduled post